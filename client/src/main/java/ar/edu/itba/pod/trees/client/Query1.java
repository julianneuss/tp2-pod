package ar.edu.itba.pod.trees.client;

import ar.edu.itba.pod.trees.api.query1.Query1Combiner;
import ar.edu.itba.pod.trees.api.query1.Query1Mapper;
import ar.edu.itba.pod.trees.api.query1.Query1Reducer;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.*;
import com.hazelcast.mapreduce.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import static ar.edu.itba.pod.trees.client.Utils.*;
import static java.util.stream.Collectors.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static java.util.Map.Entry.comparingByValue;


// El orden de impresión es descendente por el total de árboles por habitante y luego alfabético por nombre de barrio.
//        El total de árboles por habitante debe imprimirse truncado con dos decimales.


// ./query1 -Dcity=BUE -Daddresses='10.6.0.1:5701' -DinPath=. -DoutPath=.


// NOSE SI LLENAR UN MAP O ARRAY DE NEIGH NIDEA
public class Query1 {

    private static Timestamp startCSV, endCSV, startQuery, endQuery;
    private static String fileNameTime;
    private static String cityS;
    private static HashMap<String, Integer> nMap;

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException, ExecutionException, InterruptedException {

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        Options options = new Options();

        Option ip = new Option("Daddresses", "Daddresses", true, "Server IP addresses");
        ip.setRequired(true);
        options.addOption(ip);

        Option pollingPlace = new Option("Dcity", "Dcity", true, "Terget city");
        pollingPlace.setRequired(false);
        options.addOption(pollingPlace);

        Option province = new Option("DinPath", "DinPath", true, "Path for input files");
        province.setRequired(false);
        options.addOption(province);

        Option path = new Option("DoutPath", "DoutPath", true, "Path for output files");
        path.setRequired(true);
        options.addOption(path);

        cmd = parser.parse(options, args);

        cityS = cmd.getOptionValue("Dcity");
        String inputFilePath = cmd.getOptionValue("DinPath");
        String outputFilePath = cmd.getOptionValue("DoutPath");
        String serverAddresses = cmd.getOptionValue("Daddresses");

        fileNameTime = outputFilePath + "/query1" + cityS + "rta-TIMESTAMP.csv";
        startLog(fileNameTime);

        String arbolesFilePath;
        String barriosFilePath;
        if(cityS.equals("BUE")) {
            arbolesFilePath = inputFilePath + "/arbolesBUE.csv" ;
            barriosFilePath = inputFilePath + "/barriosBUE.csv" ;
        } else if (cityS.equals("VAN")) {
            arbolesFilePath = inputFilePath + "/arbolesVAN.csv" ;
            barriosFilePath = inputFilePath + "/barriosVAN.csv" ;
        } else {
            System.out.println("Wrong city parameter");
            return;
        }

        ArrayList<String> addresses = new ArrayList<>();
        StringTokenizer multiTokenizer = new StringTokenizer(serverAddresses, ";");
        while (multiTokenizer.hasMoreTokens())
        {
            addresses.add(multiTokenizer.nextToken());
        }

        ////// Instanciating HazelCast with NO CONFIG
//        Config config = new XmlConfigBuilder().build();
//        HazelcastInstance hz = Hazelcast.newHazelcastInstance(config);

        HazelcastInstance hz = HazelcastClient.newHazelcastClient(getClientConfig(addresses));


        /// PARSE CSV TO IMAP
        readNeighbourhoodsFromCSV(arbolesFilePath, barriosFilePath, hz);


        /// JOB -> FIRST MAP-REDUCE  ( Exchangable for CSV PARSER Alternative )
        JobTracker j1 = hz.getJobTracker("query1p1");
        final IMap<Integer, String> imap = hz.getMap("query1_trees");
        final KeyValueSource<Integer, String> source = KeyValueSource.fromMap(imap);
        Job<Integer, String> job = j1.newJob(source);
        startQuery = new Timestamp(System.currentTimeMillis());

        writeLog(fileNameTime,"Start MapReduce Job",startQuery);
        System.out.println("start query");

        ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query1Mapper(cityS))
                .combiner( new Query1Combiner())
                .reducer(new Query1Reducer())
                .submit();

        Map<String, Integer> treesxneigh = future.get();
        Map<String, Float> result = new HashMap<>();
        float a, b;
        for (String comuna: nMap.keySet()) {
            a = treesxneigh.get(comuna);
            b =nMap.get(comuna);
            result.put(comuna, round(a/b,2) );
            }

        Map<String, Float > resultposta = result.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        System.out.println(resultposta);


        endQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"End MapReduce Job", endQuery);
        String fileName = outputFilePath + "/query1rta"+ cityS +".csv";
        writeCSVquery1(fileName, resultposta);
        imap.clear();

        hz.shutdown();
    }



    public static float round(float d, int decimalPlace)
    {
        return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
    }

    // CSV PARSER FOR LINE OUTPUT
    public static void readNeighbourhoodsFromCSV(String fileTrees,String fileBarrios, HazelcastInstance hz) {
        boolean headers = true; int aux = 0;
        IMap<Integer, String> tMap = hz.getMap("query1_trees");
        nMap = new HashMap<>();
        System.out.println("startCSV parser");
        startCSV = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start CSV Reading", startCSV);
        try {
            BufferedReader trees_reader = new BufferedReader(new FileReader(fileTrees));
            String line = null;
            while ((line = trees_reader.readLine()) != null) {
                if(!headers)tMap.put(aux++, line);
                else headers = false;
            }
        }
        catch (IOException ex) { System.out.println("Error while reading CSV file");
            }

        headers= true;
        CSVParser csvParser = null;
        try {
            Reader reader = Files.newBufferedReader(Paths.get(fileBarrios));
            csvParser = new CSVParser(reader, CSVFormat.newFormat(';'));
            for (CSVRecord csvRecord : csvParser) {
                if(!headers)nMap.put(csvRecord.get(0) ,Integer.valueOf(csvRecord.get(1)));
                else headers = false;
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }
        System.out.println("endCSV parser");
        endCSV = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"End CSV Reading ", endCSV);
    }
}

