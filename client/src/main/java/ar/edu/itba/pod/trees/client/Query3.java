package ar.edu.itba.pod.trees.client;

import ar.edu.itba.pod.trees.api.query3.Query3Mapper;
import ar.edu.itba.pod.trees.api.query3.Query3ReducerFactory;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import javax.xml.bind.SchemaOutputResolver;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import static ar.edu.itba.pod.trees.client.Utils.*;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.*;
import static java.util.Map.Entry.*;


import java.io.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Query3 {

    private static Timestamp startCSV, endCSV, startQuery, endQuery;
    private static String fileNameTime;

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException, ExecutionException, InterruptedException {

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        Options options = new Options();

        Option ip = new Option("Daddresses", "Daddresses", true, "Server IP addresses");
        ip.setRequired(true);
        options.addOption(ip);

        Option dCity = new Option("Dcity", "Dcity", true, "Terget city");
        dCity.setRequired(false);
        options.addOption(dCity);

        Option inPath = new Option("DinPath", "DinPath", true, "Path for input files");
        inPath.setRequired(false);
        options.addOption(inPath);

        Option path = new Option("DoutPath", "DoutPath", true, "Path for output files");
        path.setRequired(true);
        options.addOption(path);

        Option firstn = new Option("Dn", "Dn", true, "Primeras n especies a listar");
        firstn.setRequired(true);
        options.addOption(firstn);

        cmd = parser.parse(options, args);

        String cityS = cmd.getOptionValue("Dcity");
        String inputFilePath = cmd.getOptionValue("DinPath");
        String outputFilePath = cmd.getOptionValue("DoutPath");
        String serverAddresses = cmd.getOptionValue("Daddresses");
        String firstN = cmd.getOptionValue("Dn");

        fileNameTime = outputFilePath + "/query3" + cityS + "rta-TIMESTAMP.csv";
        startLog(fileNameTime);

        String arbolesFilePath;
        String barriosFilePath;
        if(cityS.equals("BUE")) {
            arbolesFilePath = inputFilePath + "/arbolesBUE.csv" ;
            barriosFilePath = inputFilePath + "/barriosBUE.csv" ;
        } else if (cityS.equals("VAN")) {
            arbolesFilePath = inputFilePath + "/arbolesVAN.csv" ;
            barriosFilePath = inputFilePath + "/barriosVAN.csv" ;
        } else {
            System.out.println("Wrong city parameter");
            return;
        }

        // ERROR - IP ADDRESS EQUIVOCADO
        if(serverAddresses.equals("")){
            System.out.println("Wrong IP Address parameter");
            return;
        }
        // ERROR - N EQUIVOCADA
        if (Integer.parseInt(firstN) <= 0) {
            System.out.println("Wrong n parameter");
            return;
        }

        ArrayList<String> addresses = new ArrayList<>();
        StringTokenizer multiTokenizer = new StringTokenizer(serverAddresses, ";");
        while (multiTokenizer.hasMoreTokens())
        {
            addresses.add(multiTokenizer.nextToken());
        }

        HazelcastInstance hz = HazelcastClient.newHazelcastClient(getClientConfig(addresses));

        ////// Instanciating HazelCast with NO CONFIG
        //Config config = new XmlConfigBuilder().build();
        //HazelcastInstance hz = Hazelcast.newHazelcastInstance(config);

        /// PARSE CSV TO IMAP
        query3FromCSV(arbolesFilePath, hz);
        /// JOB -> FIRST MAP-REDUCE  ( Exchangable for CSV PARSER Alternative )
        JobTracker j1 = hz.getJobTracker("query3");
        final IMap<Integer, String> imap = hz.getMap("query3");
        final KeyValueSource<Integer, String> source = KeyValueSource.fromMap(imap);
        Job<Integer, String> job = j1.newJob(source);
        startQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start MapReduce Job",startQuery);
        System.out.println(startQuery);
        System.out.println("Started Query3");

        JobCompletableFuture<Map<String, Float >> future = job
                .mapper(new Query3Mapper(cityS))  // mapa (name,diametro)
                .reducer(new Query3ReducerFactory()) // (nombre/s/repetir, promedio)
                .submit();
        Map<String, Float > result = future.get();
        Map<String, Float > resultposta = result.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).limit(Long.parseLong(firstN))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));

        System.out.println(resultposta);
        System.out.println(endQuery);
        System.out.println("Finished Q3");
        long milliseconds = endQuery.getTime() - startQuery.getTime();
        System.out.println(milliseconds);

        imap.clear();

        //Now we write the CSV with the answer
        String fileName = outputFilePath + "/query3" + cityS + "rta.csv";
        writeCSVquery3(fileName, resultposta);
        endQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"End MapReduce Job", endQuery);
        hz.shutdown();
    }


    // CSV PARSER FOR LINE OUTPUT
    private static void query3FromCSV(String inputFilePath, HazelcastInstance hz) {
            IMap<Integer, String> map = null;
            map = hz.getMap("query3");
            startCSV = new Timestamp(System.currentTimeMillis());
            System.out.println(startCSV);
            writeLog(fileNameTime,"Start CSV Reading", startCSV);
            System.out.println("Started reading CSV ");

            int aux = 0;
            boolean headers = true;
            try {
                BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    if(!headers){
                        map.put(aux++, line);
                    }
                    else{
                        headers = false;
                    }
                }
            } catch (IOException ex) {
                System.out.println("Error while reading CSV file.");
            }

            endCSV = new Timestamp(System.currentTimeMillis());
            writeLog(fileNameTime,"Finish CSV Reading", endCSV);

            System.out.println(endCSV);
            System.out.println("Finished reading CSV ");
            System.out.print("Total Time:  ");
            long milliseconds = endCSV.getTime() - startCSV.getTime();
            System.out.println(milliseconds);

        }
    }






