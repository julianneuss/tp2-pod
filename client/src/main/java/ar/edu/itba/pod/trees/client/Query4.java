package ar.edu.itba.pod.trees.client;

import ar.edu.itba.pod.trees.api.enums.VANFields;
import ar.edu.itba.pod.trees.api.query4.Query4Mapper;
import ar.edu.itba.pod.trees.api.query4.Query4ReducerFactory;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.*;
import com.hazelcast.mapreduce.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.sql.Timestamp;
import java.io.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static ar.edu.itba.pod.trees.client.Utils.*;
public class Query4 {

    private static Timestamp startCSV, endCSV, startQuery, endQuery;
    private static String fileNameTime;

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException, ExecutionException, InterruptedException {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("arbol");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        Options options = new Options();

        Option ip = new Option("Daddresses", "Daddresses", true, "Server IP addresses");
        ip.setRequired(true);
        options.addOption(ip);

        Option pollingPlace = new Option("Dcity", "Dcity", true, "Target city");
        pollingPlace.setRequired(false);
        options.addOption(pollingPlace);

        Option province = new Option("DinPath", "DinPath", true, "Path for input files");
        province.setRequired(false);
        options.addOption(province);

        Option path = new Option("DoutPath", "DoutPath", true, "Path for output files");
        path.setRequired(true);
        options.addOption(path);

        Option min = new Option("Dmin", "Dmin", true, "Min tree amount");
        min.setRequired(true);
        options.addOption(min);

        Option name = new Option("Dname", "Dname", true, "Tree type name");
        name.setRequired(true);
        options.addOption(name);

        cmd = parser.parse(options, args);

        String cityS = cmd.getOptionValue("Dcity");
        String inputFilePath = cmd.getOptionValue("DinPath");
        String outputFilePath = cmd.getOptionValue("DoutPath");
        String serverAddresses = cmd.getOptionValue("Daddresses");
        String minTreeAmount = cmd.getOptionValue("Dmin");
        String treeTypeName = cmd.getOptionValue("Dname");

        fileNameTime = outputFilePath + "/query4" + cityS + "rta-TIMESTAMP.csv";
        startLog(fileNameTime);


        // ERROR 2 - IP ADDRESS EQUIVOCADO
        if (serverAddresses.equals("")) {
            System.out.println("Wrong IP Address parameter");
            return;
        }

        String arbolesFilePath;
        String barriosFilePath;
        if (cityS.equals("BUE")) {
            arbolesFilePath = inputFilePath + "/arbolesBUE.csv";
            barriosFilePath = inputFilePath + "/barriosBUE.csv";
        } else if (cityS.equals("VAN")) {
            arbolesFilePath = inputFilePath + "/arbolesVAN.csv";
            barriosFilePath = inputFilePath + "/barriosVAN.csv";
        } else {
            System.out.println("Wrong city parameter");
            return;
        }

        ArrayList<String> addresses = new ArrayList<>();
        StringTokenizer multiTokenizer = new StringTokenizer(serverAddresses, ";");
        while (multiTokenizer.hasMoreTokens())
        {
            addresses.add(multiTokenizer.nextToken());
        }

        HazelcastInstance hz = HazelcastClient.newHazelcastClient(getClientConfig(addresses));

        /// PARSE CSV TO IMAP
        query4FromCSV(arbolesFilePath, hz);

        /// JOB -> FIRST MAP-REDUCE  ( Exchangable for CSV PARSER Alternative )
        JobTracker j1 = hz.getJobTracker("query4");
        final IMap<Integer, String> imap = hz.getMap("query4");
        final KeyValueSource<Integer, String> source = KeyValueSource.fromMap(imap);
        Job<Integer, String> job = j1.newJob(source);
        startQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start MapReduce Job",startQuery);
        System.out.println("Started Query4");
        ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query4Mapper(treeTypeName, cityS))
                .reducer(new Query4ReducerFactory())
                .submit();
        Map<String, Integer> result = future.get();
        List<String> finalResult = result.entrySet().stream()
                .filter(entry -> entry.getValue() >= Integer.parseInt(minTreeAmount) )
                .map(Map.Entry::getKey)
                .sorted()
                .collect(Collectors.toList());
        endQuery = new Timestamp(System.currentTimeMillis());
        System.out.println("Finished Query4");
        long milliseconds = endQuery.getTime() - startQuery.getTime();
        System.out.println(milliseconds);


        //Now we write the CSV with the answer
        String fileName = outputFilePath + "/query4" + cityS + "rta.csv";
        writeCSVquery4(fileName, finalResult);

        imap.clear();
        writeLog(fileNameTime,"End MapReduce Job", endQuery);
        hz.shutdown();
    }

    // CSV PARSER FOR LINE OUTPUT
    private static void query4FromCSV(String inputFilePath, HazelcastInstance hz) {
        IMap<Integer, String> map = null;
        map = hz.getMap("query4");
        startCSV = new Timestamp(System.currentTimeMillis());
        System.out.println(startCSV);
        writeLog(fileNameTime,"Start CSV Reading", startCSV);
        System.out.println("Started reading CSV ");

        int aux = 0;
        boolean headers = true;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if(!headers){
                    map.put(aux++, line);
                }
                else{
                    headers = false;
                }
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }

        endCSV = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Finish CSV Reading", endCSV);

        System.out.println(endCSV);
        System.out.println("Finished reading CSV ");
        System.out.print("Total Time:  ");
        long milliseconds = endCSV.getTime() - startCSV.getTime();
        System.out.println(milliseconds);

    }
}


