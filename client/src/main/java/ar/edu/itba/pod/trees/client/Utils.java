package ar.edu.itba.pod.trees.client;

import ar.edu.itba.pod.trees.api.Street;
import com.hazelcast.client.config.ClientConfig;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.*;





public class Utils {

    private static CSVPrinter csvPrinter;
    public static void writeCSVquery4(String file, List<String> results) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file));
            final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("Barrio A", "Barrio B").withRecordSeparator('\n'));

            List<String> alreadyLogged = new ArrayList<>();
            for (String s:results) {
                for (String s2 : results) {
                    if (!s.equals(s2)) {
                        if(!alreadyLogged.contains(s2)) {
                            try {
                                csvPrinter.printRecord(s, s2);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                alreadyLogged.add(s);
            }
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }
    public static void writeCSVquery3(String file, Map<String, Float> results) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file));
            final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("NOMBRE_CIENTIFICO", "PROMEDIO_DIAMETRO").withRecordSeparator('\n'));
            results.entrySet().stream().sorted((o1, o2) -> {
                if(!o1.getValue().equals(o2.getValue())) return Double.compare(o2.getValue(),o1.getValue());
                else return o1.getKey().compareTo(o2.getKey());
            }).forEach(entry -> {
                String party = entry.getKey();
                DecimalFormat format = new DecimalFormat("##.00");
                String percent = format.format(entry.getValue());
                try {
                    csvPrinter.printRecord(party, percent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }


    public static void writeCSVquery1(String file, Map<String, Float> results) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file));
            final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("BARRIO", "ARBOLES_POR_HABITANTE").withRecordSeparator('\n'));
            for (String key : results.keySet()) {
              Float value = results.get(key);
                try {
                    csvPrinter.printRecord(key, value);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }


    public static void writeCSVquery2(String file, Map<String, Street>  results) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file));
            final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("BARRIO", "CALLE_CON_MAS_ARBOLES", "ARBOLES").withRecordSeparator('\n'));
            SortedSet<String> keys = new TreeSet<>(results.keySet());
            for (String key : keys) {
                Street value = results.get(key);
                try {
                    csvPrinter.printRecord(key, value, value.getnOfTrees());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }


    public static void writeCSVquery5(String file,Map<Integer, List<String>>  results) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(file));
            final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("GRUPO", "BARRIO A", "BARRIO B").withRecordSeparator('\n'));
            SortedSet<Integer> keys = new TreeSet<>(Collections.reverseOrder());
            keys.addAll(results.keySet());
            for (Integer key : keys) {
                List<String> value = results.get(key);
                if (value.size() > 1 && key > 0) {
                    List<String> alreadyLogged = new ArrayList<>();
                    for (String s : value) {
                        for (String s2 : value) {
                            if (!s.equals(s2)) {
                                if (!alreadyLogged.contains(s2)) {
                                    csvPrinter.printRecord(key * 1000, s, s2);
                                }
                            }
                        }
                        alreadyLogged.add(s);
                    }
                }
            }
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }


    public static void startLog(String file) {
        BufferedWriter writer = null;
        try {
            writer = Files.newBufferedWriter(Paths.get(file));
            csvPrinter = new CSVPrinter(writer, CSVFormat.newFormat(';').withHeader("EVENT", "TIME").withRecordSeparator('\n'));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error while printing CSV file.");
        }
    }
    public static void writeLog(String file, String event, Timestamp time) {
        if(csvPrinter == null)
            return;

        try {
            try {
                csvPrinter.printRecord(event,time);
            } catch (IOException e) {
                e.printStackTrace();
            }
            csvPrinter.flush();
        } catch (IOException e){
            System.out.println("Error while printing CSV file.");
        }
    }

    static ClientConfig getClientConfig(ArrayList<String> addresses){
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getGroupConfig().setName("g12-cluster").setPassword("12345678");
        clientConfig.getNetworkConfig().addAddress(addresses.toArray(new String[0]));
//        clientConfig.setProperty("hazelcast.logging.type", "slf4j");
        return clientConfig;
    }
}
