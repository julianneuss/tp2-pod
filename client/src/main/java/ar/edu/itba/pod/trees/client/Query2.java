package ar.edu.itba.pod.trees.client;

import ar.edu.itba.pod.trees.api.*;
import ar.edu.itba.pod.trees.api.query2.Query2Mapper;
import ar.edu.itba.pod.trees.api.query2.Query2ReducerFactory;
import ar.edu.itba.pod.trees.api.query2.Query2SecondMapper;
import ar.edu.itba.pod.trees.api.query2.Query2SecondReducerFactory;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.util.*;


import java.io.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static ar.edu.itba.pod.trees.client.Utils.*;


public class Query2 {

    private static Timestamp startCSV, endCSV, startQuery, endQuery,startQuery2,endQuery2;
    private static String fileNameTime;

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException, ExecutionException, InterruptedException {

        CommandLineParser parser = new DefaultParser();
        Options options = new Options();

        Option ip = new Option("Daddresses", "Daddresses", true, "Server IP addresses");
        ip.setRequired(true);
        options.addOption(ip);

        Option dCity = new Option("Dcity", "Dcity", true, "Terget city");
        dCity.setRequired(false);
        options.addOption(dCity);

        Option inPath = new Option("DinPath", "DinPath", true, "Path for input files");
        inPath.setRequired(false);
        options.addOption(inPath);

        Option path = new Option("DoutPath", "DoutPath", true, "Path for output files");
        path.setRequired(true);
        options.addOption(path);

        Option min = new Option("Dmin", "Dmin", true, "mínimo número de arboles en calle.");
        min.setRequired(true);
        options.addOption(min);

        CommandLine cmd = parser.parse(options, args);

        String cityS = cmd.getOptionValue("Dcity");
        String inputFilePath = cmd.getOptionValue("DinPath");
        String outputFilePath = cmd.getOptionValue("DoutPath");
        String serverAddresses = cmd.getOptionValue("Daddresses");
        String minValueStr = cmd.getOptionValue("Dmin");
        int minValue = Integer.parseInt(minValueStr);

        fileNameTime = outputFilePath + "/query2" + cityS + "rta-TIMESTAMP.csv";
        startLog(fileNameTime);

        String arbolesFilePath;
        String barriosFilePath;
        if(cityS.equals("BUE")) {
            arbolesFilePath = inputFilePath + "/arbolesBUE.csv" ;
            barriosFilePath = inputFilePath + "/barriosBUE.csv" ;
        } else if (cityS.equals("VAN")) {
            arbolesFilePath = inputFilePath + "/arbolesVAN.csv" ;
            barriosFilePath = inputFilePath + "/barriosVAN.csv" ;
        } else {
            System.out.println("Wrong city parameter");
            return;
        }

        ArrayList<String> addresses = new ArrayList<>();
        StringTokenizer multiTokenizer = new StringTokenizer(serverAddresses, ";");
        while (multiTokenizer.hasMoreTokens())
        {
            addresses.add(multiTokenizer.nextToken());
        }


        if (minValue <= 0) {
            System.out.println("minimum value not permitted");
            return;
        }


        HazelcastInstance hz = HazelcastClient.newHazelcastClient(getClientConfig(addresses));

//        Config config = new XmlConfigBuilder().build();
//        HazelcastInstance hz = Hazelcast.newHazelcastInstance(config);

        /// PARSE CSV TO IMAP
        query2FromCSV(arbolesFilePath, hz);
        /// JOB -> FIRST MAP-REDUCE  ( Exchangable for CSV PARSER Alternative )
        JobTracker j1 = hz.getJobTracker("query2");
        final IMap<Integer, String> imap = hz.getMap("query2");
        final KeyValueSource<Integer, String> source = KeyValueSource.fromMap(imap);
        Job<Integer, String> job = j1.newJob(source);
        startQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start MapReduce Job 1",startQuery);
        System.out.println("Started Query2 - Job 1");

        Set<String> barriosSet = readBarriosFile(barriosFilePath);
        // First MapReduce returns <Street, NumOfTrees>
        JobCompletableFuture<Map<Street, Integer >> future = job
                .mapper(new Query2Mapper(barriosSet, cityS))
                .reducer(new Query2ReducerFactory())
                .submit();

        endQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"End MapReduce Job 1", endQuery);

        Map<Street, Integer> result = future.get();
        imap.clear();

        IMap<Street, Integer> map =hz.getMap("query2final");
        map.putAll(result);

        final KeyValueSource<Street, Integer> secSource = KeyValueSource.fromMap(map);
        Job<Street, Integer> secJob = j1.newJob(secSource);
        startQuery2 = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start MapReduce Job 2",startQuery2);
        // Second MapReduce returns <neighbourhood, MaxStreet>
        JobCompletableFuture<Map<String, Street >> secondFuture = secJob
                .mapper(new Query2SecondMapper(minValue))
                .reducer(new Query2SecondReducerFactory())
                .submit();
        endQuery2 = new Timestamp(System.currentTimeMillis());
        Map<String, Street > secondResult = secondFuture.get();

        System.out.println("Finished Q2 ");
        long milliseconds = endQuery2.getTime() - startQuery.getTime();
        System.out.println(milliseconds);

        String fileName = outputFilePath + "/query2rta"+ cityS +".csv";
        writeCSVquery2(fileName, secondResult);
        writeLog(fileNameTime,"End MapReduce Job 2", endQuery2);
        map.clear();
        hz.shutdown();
    }

    // CSV PARSER FOR LINE OUTPUT
    private static void query2FromCSV(String inputFilePath, HazelcastInstance hz) {
        IMap<Integer, String> map = hz.getMap("query2");
        startCSV = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start CSV Reading", startCSV);
        System.out.println("Started reading CSV ");
        int aux = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
            String line = null;
            while ((line = reader.readLine()) != null) {
                map.put(aux++, line);
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }

        endCSV = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Finish CSV Reading", endCSV);
    }


    private static Set<String> readBarriosFile(String inputFilePath) {
        Set<String> set = new HashSet<>();

        Reader targetReader;
        CSVParser csvParser;
        int aux = 0;
        boolean headers = true;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if(!headers){
                    targetReader = new StringReader(line);
                    csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
                    for (CSVRecord csvRecord : csvParser) {
                        String neighbourhoodName = csvRecord.get(0);
                        set.add(neighbourhoodName);
                    }

                }
                else{
                    headers = false;
                }
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }

        return set;
    }



}
