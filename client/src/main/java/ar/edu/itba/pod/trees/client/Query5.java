package ar.edu.itba.pod.trees.client;

import ar.edu.itba.pod.trees.api.Query5.*;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.*;
import com.hazelcast.mapreduce.*;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.sql.Timestamp;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static ar.edu.itba.pod.trees.client.Utils.*;


//  ./query4 -Dcity=VAN -Daddresses='10.6.0.1:5701' -DinPath=. -DoutPath=.

//Pares de barrios que registran la misma cantidad de miles de árboles

// me importa solo cantidad de arboles y comuna
public class Query5 {

    private static Timestamp startCSV, endCSV, startQuery, endQuery, startQuery2, endQuery2;
    private static String fileNameTime;

    public static void main(String[] args) throws ParseException, RemoteException, NotBoundException, MalformedURLException, ExecutionException, InterruptedException {

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        Options options = new Options();

        Option ip = new Option("Daddresses", "Daddresses", true, "Server IP addresses");
        ip.setRequired(true);
        options.addOption(ip);

        Option pollingPlace = new Option("Dcity", "Dcity", true, "Terget city");
        pollingPlace.setRequired(false);
        options.addOption(pollingPlace);

        Option province = new Option("DinPath", "DinPath", true, "Path for input files");
        province.setRequired(false);
        options.addOption(province);

        Option path = new Option("DoutPath", "DoutPath", true, "Path for output files");
        path.setRequired(true);
        options.addOption(path);

        cmd = parser.parse(options, args);

        String cityS = cmd.getOptionValue("Dcity");
        String inputFilePath = cmd.getOptionValue("DinPath");
        String outputFilePath = cmd.getOptionValue("DoutPath");
        String serverAddresses = cmd.getOptionValue("Daddresses");

        fileNameTime = outputFilePath + "/query5" + cityS + "rta-TIMESTAMP.csv";
        startLog(fileNameTime);

        StringTokenizer multiTokenizer = new StringTokenizer(serverAddresses, ";");
        ArrayList<String> addresses = new ArrayList<>();
        while (multiTokenizer.hasMoreTokens())
        {
           addresses.add(multiTokenizer.nextToken());
        }

        String arbolesFilePath;
        String barriosFilePath;
        if(cityS.equals("BUE")) {
            arbolesFilePath = inputFilePath + "/arbolesBUE.csv" ;
            barriosFilePath = inputFilePath + "/barriosBUE.csv" ;
        } else if (cityS.equals("VAN")) {
            arbolesFilePath = inputFilePath + "/arbolesVAN.csv" ;
            barriosFilePath = inputFilePath + "/barriosVAN.csv" ;
        } else {
            System.out.println("Wrong city parameter");
            return;
        }


        //// Instanciating HazelCast with NO CONFIG
        HazelcastInstance hz = HazelcastClient.newHazelcastClient(getClientConfig(addresses));
//        Config config = new XmlConfigBuilder().build();
//        HazelcastInstance hz = Hazelcast.newHazelcastInstance(config);


        /// PARSE CSV TO IMAP
        readNeighbourhoodsFromCSV(arbolesFilePath, hz);


        /// JOB -> FIRST MAP-REDUCE  ( Exchangable for CSV PARSER Alternative )
        JobTracker j1 = hz.getJobTracker("query5");
        final IMap<Integer, String> imap = hz.getMap("query5");
        final KeyValueSource<Integer, String> source = KeyValueSource.fromMap(imap);
        Job<Integer, String> job = j1.newJob(source);
        startQuery = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start MapReduce Job 1",startQuery);
        System.out.println("Started Query1 ");
        ICompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query5Mapper(cityS))
                .combiner( new Query5Combiner())
                .reducer(new Query5Reducer())
                .submit();

        Map<String, Integer> result = future.get();
        endQuery = new Timestamp(System.currentTimeMillis());
        System.out.println("Finished Q1 ");
        long milliseconds = endQuery.getTime() - startQuery.getTime();
        System.out.println(milliseconds);
        IMap<String, Integer> aux = hz.getMap("query5p2");
        aux.putAll(result);
        writeLog(fileNameTime,"End MapReduce Job 1", endQuery);



        JobTracker j2 = hz.getJobTracker("query52");
        final IMap<String, Integer> imap2 = hz.getMap("query5p2");
        final KeyValueSource<String, Integer> source2 = KeyValueSource.fromMap(imap2);
        Job<String, Integer> job2 = j2.newJob(source2);
        startQuery2 = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Start MapReduce Job 2",startQuery2);
        System.out.println("Started Query2 ");
        ICompletableFuture<Map<Integer, List<String>>> future2 = job2
                .mapper(new Query5SecondMapper())
                .reducer(new Query5SecondReducer())
                .submit();

        Map<Integer, List<String>> result2 = future2.get();
        for( Integer group :  result2.keySet() ) {
            result2.put(group, result2.get(group).stream().sorted().collect(Collectors.toList()));
        }

        endQuery2 = new Timestamp(System.currentTimeMillis());
        System.out.println("Finished Q2 ");
        long milliseconds2 = endQuery2.getTime() - startQuery2.getTime();
        System.out.println(milliseconds2);
        System.out.println(result2);

        imap.clear();
        imap2.clear();

        String fileName = outputFilePath + "/query5rta"+ cityS +".csv";
        writeCSVquery5(fileName, result2);
        writeLog(fileNameTime,"End MapReduce Job 2", endQuery2);
        hz.shutdown();

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////



    /////////////////////////// CSV //////////////////////////////////////////


    // CSV PARSER FOR LINE OUTPUT
    private static void readNeighbourhoodsFromCSV(String file, HazelcastInstance hz) {
        IMap<Integer, String> map = hz.getMap("query5");
        startCSV = new Timestamp(System.currentTimeMillis());
        System.out.println(startCSV);
        writeLog(fileNameTime,"Start CSV Reading", startCSV);
        System.out.println("Started reading CSV ");

        int aux = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                map.put(aux++, line);
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }

        endCSV = new Timestamp(System.currentTimeMillis());
        writeLog(fileNameTime,"Finish CSV Reading", endCSV);
        System.out.println("Finished reading CSV ");
        System.out.print("Total Time:  ");
        long milliseconds = endCSV.getTime() - startCSV.getTime();
        System.out.println(milliseconds);
    }
}

