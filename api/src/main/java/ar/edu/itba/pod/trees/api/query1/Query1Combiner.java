package ar.edu.itba.pod.trees.api.query1;

import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

public class Query1Combiner implements CombinerFactory<String, Integer, Integer> {
    @Override
    public Combiner<Integer, Integer> newCombiner(String key) {
        return new NeighTreeCountCombiner();
    }

    private class NeighTreeCountCombiner extends Combiner<Integer, Integer> {
        private Integer sum = 0;

        @Override
        public void combine(Integer value) {
            sum++;
        }

        @Override
        public Integer finalizeChunk() {
            return sum;
        }

        @Override
        public void reset() {
            sum = 0;
        }
    }
}