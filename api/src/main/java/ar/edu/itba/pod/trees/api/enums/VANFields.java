package ar.edu.itba.pod.trees.api.enums;

public enum VANFields {
    STD_STREET(2),
    COMMON_NAME(6),
    NEIGHBOURHOOD_NAME(12),
    DIAMETER(15);

    private final int value;

    VANFields(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}