package ar.edu.itba.pod.trees.api.query2;

import ar.edu.itba.pod.trees.api.Street;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query2SecondReducerFactory implements ReducerFactory<String, Street, Street > {
    @Override
    public Reducer<Street, Street > newReducer(String key) {
        return new Query2Reducer(key);
    }

    private static class Query2Reducer extends Reducer<Street, Street> {
        private Street streetMax;
        String neighbourhood;

        public Query2Reducer(String neighbourhood) {
            this.neighbourhood = neighbourhood;
        }

        @Override
        public void beginReduce() {
            streetMax = new Street("", neighbourhood);
            streetMax.setnOfTrees(0);
        }

        @Override
        public void reduce(Street value) {
            if(value.getnOfTrees() > streetMax.getnOfTrees()) {
                streetMax.setnOfTrees(value.getnOfTrees());
                streetMax.setName(value.getName());
            }
        }

        @Override
        public Street finalizeReduce() {
            return streetMax ;
        }}
}
