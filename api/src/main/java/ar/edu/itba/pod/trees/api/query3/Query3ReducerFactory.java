package ar.edu.itba.pod.trees.api.query3;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query3ReducerFactory implements ReducerFactory<String, Float, Float > {
    @Override
    public Reducer<Float, Float > newReducer(String key) {
        return new Query3Reducer();
    }

    private class Query3Reducer extends Reducer<Float, Float> {
        private volatile float diam;
        private volatile float counter;

        @Override
        public void beginReduce() {
            diam=0;
            counter=0;
        }

        @Override
        public void reduce(Float value) {
            diam+=value;
            counter++;
        }

        @Override
        public Float finalizeReduce() {
            return diam/counter ;
        }
    }
}
