package ar.edu.itba.pod.trees.api.query2;

import ar.edu.itba.pod.trees.api.Street;
import ar.edu.itba.pod.trees.api.enums.BUEFields;
import ar.edu.itba.pod.trees.api.enums.VANFields;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Set;

public class Query2Mapper implements Mapper<Integer, String, Street, Integer> {
    int STREET_NAME_FIELD;
    int NEIGHBOURHOOD_NAME_FIELD;
    Set<String> barriosSet;

    public Query2Mapper(Set<String> barriosSet, String city) {
        this.barriosSet = barriosSet;
        if(city.equals("BUE")) {
            STREET_NAME_FIELD = BUEFields.CALLE_NOMBRE.getValue();
            NEIGHBOURHOOD_NAME_FIELD = BUEFields.COMUNA.getValue();
        } else {
            STREET_NAME_FIELD = VANFields.STD_STREET.getValue();
            NEIGHBOURHOOD_NAME_FIELD = VANFields.NEIGHBOURHOOD_NAME.getValue();
        }
    }

    @Override
    public void map(Integer linen, String line, Context<Street,Integer> context) {
        CSVParser csvParser = null;
        int aux= 0;
        try {
            Reader targetReader = new StringReader(line);
            csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
            for (CSVRecord csvRecord : csvParser) {
                aux++;
                String streetName = csvRecord.get(STREET_NAME_FIELD);
                String neighbourhoodName = csvRecord.get(NEIGHBOURHOOD_NAME_FIELD);
                if( barriosSet.contains(neighbourhoodName) ) {
                    context.emit(new Street(streetName, neighbourhoodName), 1);
                }
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }
    }
}