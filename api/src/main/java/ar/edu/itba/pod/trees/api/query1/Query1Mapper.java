package ar.edu.itba.pod.trees.api.query1;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class Query1Mapper implements Mapper<Integer, String, String, Integer> {
    private final Integer ONE = 1;
    private final String city;

    public Query1Mapper(String city) {
        this.city= city;
    }
    @Override
    public void map(Integer line, String key, Context<String, Integer> context) {
        CSVParser csvParser = null;
        String linea = null;
        try {
            Reader targetReader = new StringReader(key);
            csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
            for (CSVRecord csvRecord : csvParser) {
                if(city.equals("BUE")){
                    linea = csvRecord.get(2);
                }
                else if(city.equals("VAN")){
                    linea = csvRecord.get(12);
                }
                context.emit(linea, ONE);
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }
    }
}