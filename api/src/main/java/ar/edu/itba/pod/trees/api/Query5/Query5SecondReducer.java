package ar.edu.itba.pod.trees.api.Query5;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.util.ArrayList;
import java.util.List;

public class Query5SecondReducer implements ReducerFactory<Integer, String, List<String>> {
    @Override
    public Reducer<String, List<String>> newReducer(Integer key) {
        return new WordCountReducer();
    }

    private class WordCountReducer extends Reducer<String,  List<String>> {
        private volatile List<String> arr;

        @Override
        public void beginReduce() {
            arr = new ArrayList<>();
        }

        @Override
        public void reduce(String value) {
            arr.add(value);
        }

        @Override
        public List<String> finalizeReduce() {
            return arr;
        }
    }

}

