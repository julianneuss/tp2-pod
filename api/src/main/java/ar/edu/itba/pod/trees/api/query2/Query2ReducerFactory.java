package ar.edu.itba.pod.trees.api.query2;

import ar.edu.itba.pod.trees.api.Street;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query2ReducerFactory implements ReducerFactory<Street, Integer, Integer > {
    @Override
    public Reducer<Integer, Integer > newReducer(Street key) {
        return new Query2Reducer();
    }

    private static class Query2Reducer extends Reducer<Integer, Integer> {
        private volatile Integer counter;

        @Override
        public void beginReduce() {
            counter=0;
        }

        @Override
        public void reduce(Integer value) {
            counter++;
        }

        @Override
        public Integer finalizeReduce() {
            return counter ;
        }}
}