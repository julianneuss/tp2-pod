package ar.edu.itba.pod.trees.api.query4;

import ar.edu.itba.pod.trees.api.enums.BUEFields;
import ar.edu.itba.pod.trees.api.enums.VANFields;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class Query4Mapper implements Mapper<Integer, String, String, Integer> {
    private final String treeName;
    int nameField;
    int neighbourhoodField;

    public Query4Mapper(String name, String city) {
        this.treeName = name;
        if(city.equals("BUE")){
            nameField = BUEFields.NOMBRE_CIENTIFICO.getValue();
            neighbourhoodField = BUEFields.COMUNA.getValue();
        }
        if(city.equals("VAN")){
            nameField = VANFields.COMMON_NAME.getValue();
            neighbourhoodField = VANFields.NEIGHBOURHOOD_NAME.getValue();
        }
    }

    @Override
    public void map(Integer line, String key, Context<String, Integer> context) {
        Integer ONE = 1;
        CSVParser csvParser = null;
        try {
            Reader targetReader = new StringReader(key);
            csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
            for (CSVRecord csvRecord : csvParser) {
                if(csvRecord.get(nameField).equals(treeName)){
                    context.emit(csvRecord.get(neighbourhoodField), ONE);
                }
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }


    }
}