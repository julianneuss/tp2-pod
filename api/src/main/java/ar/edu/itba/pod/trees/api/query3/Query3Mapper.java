package ar.edu.itba.pod.trees.api.query3;

import ar.edu.itba.pod.trees.api.enums.BUEFields;
import ar.edu.itba.pod.trees.api.enums.VANFields;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/// MAPPER THAT EMITS (name, diameter)
public class Query3Mapper implements Mapper<Integer, String, String, Float> {
    int nameField;
    int diameterField;

    public Query3Mapper(String cityField) {
        if(cityField.equals("BUE")){
            nameField = BUEFields.NOMBRE_CIENTIFICO.getValue();
            diameterField = BUEFields.DIAMETRO.getValue();
        }
        if(cityField.equals("VAN")){
            nameField = VANFields.COMMON_NAME.getValue();
            diameterField = VANFields.DIAMETER.getValue();
        }
    }

    @Override
    public void map(Integer linen, String line, Context<String,Float> context) {
        CSVParser csvParser = null;

        try {
            Reader targetReader = new StringReader(line);
            csvParser = new CSVParser(targetReader, CSVFormat.newFormat(';'));
            for (CSVRecord csvRecord : csvParser) {
                String name = csvRecord.get(nameField);//6
                String diameter = csvRecord.get(diameterField);//15
                context.emit(name, Float.parseFloat(diameter));
            }
        } catch (IOException ex) {
            System.out.println("Error while reading CSV file.");
        }

    }

}