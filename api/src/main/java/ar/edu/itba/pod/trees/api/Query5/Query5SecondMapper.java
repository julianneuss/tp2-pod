package ar.edu.itba.pod.trees.api.Query5;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query5SecondMapper implements Mapper<String, Integer, Integer, String> {
    private final Integer ONE = 1;
    @Override
    public void map(String line, Integer key, Context<Integer, String> context) {
        context.emit(key/1000, line);
    }

}

