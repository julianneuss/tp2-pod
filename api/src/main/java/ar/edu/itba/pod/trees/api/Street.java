package ar.edu.itba.pod.trees.api;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;
import java.util.Objects;

public class Street implements DataSerializable {
    private String name;
    private String neighbourhoodName;
    private Integer nOfTrees;

    public Street() {
    }

    public Street(String name, String neighbourhoodName) {
        this.name = name;
        this.neighbourhoodName = neighbourhoodName;
        this.nOfTrees = 0;
    }

    public String getName() {
        return name;
    }

    public String getNeighbourhoodName() {
        return neighbourhoodName;
    }

    public Integer getnOfTrees() {
        return nOfTrees;
    }

    public void setnOfTrees(Integer nOfTrees) {
        this.nOfTrees = nOfTrees;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Street street = (Street) o;
        return name.equals(street.name) &&
                neighbourhoodName.equals(street.neighbourhoodName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, neighbourhoodName);
    }

    @Override
    public void writeData(ObjectDataOutput objectDataOutput) throws IOException {
        objectDataOutput.writeUTF(name);
        objectDataOutput.writeUTF(neighbourhoodName);
        objectDataOutput.writeInt(nOfTrees);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void readData(ObjectDataInput objectDataInput) throws IOException {
        name = objectDataInput.readUTF();
        neighbourhoodName = objectDataInput.readUTF();
        nOfTrees = objectDataInput.readInt();
    }
}
