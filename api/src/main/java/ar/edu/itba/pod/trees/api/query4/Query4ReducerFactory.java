package ar.edu.itba.pod.trees.api.query4;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query4ReducerFactory implements ReducerFactory<String, Integer, Integer> {
    @Override
    public Reducer<Integer, Integer> newReducer(String key) {
        return new Query4Reducer();
    }

    private static class Query4Reducer extends Reducer<Integer, Integer> {
        private volatile int total;

        @Override
        public void beginReduce() {
            total=0;
        }

        @Override
        public void reduce(Integer i) {
            total++;
        }

        @Override
        public Integer finalizeReduce() {
            return total;
        }}
}
