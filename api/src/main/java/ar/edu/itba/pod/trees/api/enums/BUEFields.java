package ar.edu.itba.pod.trees.api.enums;

public enum BUEFields {
    COMUNA(2),
    CALLE_NOMBRE(4),
    NOMBRE_CIENTIFICO(7),
    DIAMETRO(11);

    private final int value;

    BUEFields(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}