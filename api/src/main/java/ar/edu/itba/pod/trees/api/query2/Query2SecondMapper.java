package ar.edu.itba.pod.trees.api.query2;

import ar.edu.itba.pod.trees.api.Street;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query2SecondMapper implements Mapper<Street, Integer, String, Street> {
    Integer minValue;

    public Query2SecondMapper(Integer minValue) {
        this.minValue = minValue;
    }

    @Override
    public void map(Street street, Integer nOfTrees, Context<String,Street> context) {
        if ( nOfTrees >= minValue ) {
            street.setnOfTrees(nOfTrees);
            context.emit(street.getNeighbourhoodName(), street);
        }
    }
}