package ar.edu.itba.pod.trees.server;

import com.hazelcast.core.Hazelcast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.core.HazelcastInstance;

public class Server {

    public static void main(String[] args) {
        System.out.println("Intantiating a new Hazelcast Node.... ");
        HazelcastInstance hz = Hazelcast.newHazelcastInstance();
    }
}
