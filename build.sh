#!/bin/bash

mvn install;

tar -xzf server/target/tpe2-g12-server-1.0-SNAPSHOT-bin.tar.gz
tar -xzf client/target/tpe2-g12-client-1.0-SNAPSHOT-bin.tar.gz

chmod u+x tpe2-g12-client-1.0-SNAPSHOT/query*
chmod u+x tpe2-g12-server-1.0-SNAPSHOT/run-*